﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.UI;

public class ObjScript : MonoBehaviour
{

    public enum State
    {
        On_Target,
        Lost_Target
    }
    public State curState = State.On_Target;
    private Vector3 myScale;
    private float ypos;
    public Vector3 targetLostRot;
    public float offset,zPos;
    private GameObject destPos;
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        myScale = transform.localScale;
        ypos = transform.localPosition.y;
        audioSource = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(curState == State.Lost_Target)
        {
            Vector3 destPos = new Vector3(Controller.Instance.endPos.transform.position.x,
                Controller.Instance.endPos.transform.position.y + offset,
                Controller.Instance.endPos.transform.position.z + zPos);

            transform.position = Vector3.Lerp(transform.position, destPos, Time.deltaTime * 10);
        }

    }

    public void AudioPlay()
    {
        audioSource.Play();
    }

    public void AudioStop()
    {
        audioSource.Stop();
    }

    // Click delete
    public void OnClickDel ()
    {
		// Hide the object.
        DoDisable();

		// Stop playing audio 
        AudioStop();
    }

    public void OnFoundTarget()
    {
        curState = State.On_Target;

        ResetAll();

        DoEnable();

        AudioStop();

	}

	public void OnLostTarget()
    {
        curState = State.Lost_Target;

        transform.position = Controller.Instance.startPos.transform.position;

        transform.rotation = Quaternion.Euler(targetLostRot);
        
    }


    public void btnClose()
    {
        curState = State.Lost_Target;
    }

    public void ResetPos()
    {
        transform.localPosition = new Vector3(0, ypos, 0);
    }

    public void ResetRot()
    {
        transform.localRotation  = Quaternion.identity;

    }

    public void ResetScale()
    {
        transform.localScale = myScale;
    }

    public void ResetAll()
    {
        ResetPos();
        ResetRot();
        ResetScale();
    }

    public void DoEnable()
    {
        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);

        foreach (var component in rendererComponents)
            component.enabled = true;

        foreach (var component in colliderComponents)
            component.enabled = true;

        foreach (var component in canvasComponents)
            component.enabled = true;
            
    }

    public void DoDisable()
    {
        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);

        foreach (var component in rendererComponents)
            component.enabled = false;

        foreach (var component in colliderComponents)
            component.enabled = false;

        foreach (var component in canvasComponents)
            component.enabled = false;

    }

    public void OnDisableObj()
    {
        DoDisable();
    }

}
