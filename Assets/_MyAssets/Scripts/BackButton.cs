﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackButton : MonoBehaviour {

    Scene curScene;

    // Use this for initialization
    void Start () {

        curScene = SceneManager.GetActiveScene();
	}
	
	// Update is called once per frame
	void Update () {
        if (curScene.name == "MainScene")
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }

        }

        else if (curScene.name== "ARScene" /*|| curScene.name == "QuizScene"*/)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                //SceneManager.LoadScene(1);
				GetComponent<SceneChange>().LoadScene(1);
            }
        }

    }

    public void Back()
    {
        SceneManager.LoadScene(1);
    }
}
