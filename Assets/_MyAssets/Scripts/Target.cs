﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class Target : MonoBehaviour, ITrackableEventHandler
{
	private TrackableBehaviour mTrackableBehaviour;
	public GameObject showObj;
	private bool isTargetFound = false;

	void Start()
	{
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
	}

	// Update is called once per frame
	void Update()
	{

	}

	public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			isTargetFound = true;

			Controller.Instance.FoundNewTarget(showObj);

			showObj.GetComponent<ObjScript>().OnFoundTarget();
		}

		else
		{
			transform.rotation = Quaternion.identity;

			if (isTargetFound)
			{
				showObj.GetComponent<ObjScript>().OnLostTarget();

			}
			else
			{
				showObj.GetComponent<ObjScript>().OnDisableObj();

			}
		}
	}
}
