﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class SceneChange : MonoBehaviour
{
    public GameObject BG;
    public Slider slider;
    public Text txtPercentage;

    public void LoadScene(int sceneIndex)
    {
        StartCoroutine(LevelSync(sceneIndex)); 
    }

    private IEnumerator LevelSync(int sceneIndex)
    {
        yield return new WaitForSeconds(1.01f);

        AsyncOperation async = SceneManager.LoadSceneAsync(sceneIndex);

        BG.SetActive(true);

		if(sceneIndex == 2)
        this.GetComponent<AudioSource>().Stop();

        Debug.Log(async.progress);

        while (!async.isDone)
        {
            float progress = Mathf.Clamp01(async.progress / .9f);
            slider.value = progress;
            progress *= 100;
            int i = Mathf.RoundToInt(progress);
            
            txtPercentage.text = "Loading... " + i + "%";
            yield return null;
        }

    }

}
