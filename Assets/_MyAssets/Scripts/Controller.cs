﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Controller : MonoBehaviour
{
    #region Singleton
    private static Controller _instance;

    public static Controller Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("Controller");
                go.AddComponent<Controller>();
            }

            return _instance;
        }
    }
    #endregion

    public GameObject arCam;
	public GameObject curShowingObj;
	public GameObject startPos, endPos;
	private Vector3 endStartPos;

	private void Awake()
	{
		_instance = this;
	}

	// Start is called before the first frame update
	void Start()
	{
		Application.targetFrameRate = 60;
		//endStartPos = endPos.transform.localPosition;
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			arCam.transform.rotation =
				Quaternion.Euler(arCam.transform.rotation.x, arCam.transform.rotation.y,
				arCam.transform.eulerAngles.z + 90);
		}
	}

	public void ResetCam()
	{
		if (arCam == null)
			return;

		arCam.transform.position = Vector3.zero;
		arCam.transform.rotation = Quaternion.identity;

	}


	public void FoundNewTarget(GameObject newObj)
	{
		//endPos.transform.localPosition = endStartPos;


		if (curShowingObj == null)
		{
			curShowingObj = newObj;
			return;
		}
		else
		{
			if (curShowingObj != newObj)
			{
				if (curShowingObj != null)
				{
					
					curShowingObj.GetComponent<ObjScript>().OnClickDel();

				}
				curShowingObj = newObj;

			}
			else
			{
				curShowingObj.GetComponent<ObjScript>().OnFoundTarget();

			}
		}
	}

	public void OnClickDel()
	{
		if (curShowingObj == null)
			return;

		curShowingObj.GetComponent<ObjScript>().OnClickDel();
	}
}
